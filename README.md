# .val's.dot.files :3

- managed using the git `--work-tree` trick described [here](https://mitxela.com/projects/dotfiles_management) (but just for `$HOME`)
- feel free to steal stuff obviously that's why it's public
  - no specific license for the whole repo as this is not a Piece of Software, files can have licenses inside
- not guaranteed to 100% support things that aren't FreeBSD
- I type on [Colemak](https://colemak.com/), keymaps are all made for that
