let-env PROMPT_COMMAND = { $env.PWD | str replace -s $nu.home-path '~' }
let-env PROMPT_COMMAND_RIGHT = { (if $env.LAST_EXIT_CODE != 0 {
  $"(ansi red) ^($env.LAST_EXIT_CODE)^ "
} else { "" }) + (ansi blue) + (date now | date format '%R') }

let-env ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

let-env PATH = ($env.PATH | split row (char esep)
  | prepend $"($nu.home-path)/.config/netlify/helper/bin"
  | prepend $"($nu.home-path)/.cargo/bin"
  | prepend $"($nu.home-path)/.local/bin"
  | where ($it | path exists)
)

let-env EDITOR = "hx"
let-env CARGO_INSTALL_ROOT = $"($nu.home-path)/.local"

# Convert C shell setenv lines to record
# https://github.com/nushell/nushell/issues/2812#issuecomment-1105590343
def csh2env [] { lines | parse "setenv {name} {value};" | reduce -f {} { |it, acc| $acc | upsert $it.name $it.value } }
if ("SSH_AUTH_SOCK" not-in $env) { ssh-agent -c | csh2env } else { {} } | load-env
