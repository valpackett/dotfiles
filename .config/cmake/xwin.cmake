# https://github.com/Jake-Shadle/xwin
# xwin splat --preserve-ms-arch-notation --include-debug-libs --output ~/.local/share/xwin

set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_PROCESSOR AMD64)

set(CMAKE_C_COMPILER clang-cl)
set(CMAKE_CXX_COMPILER clang-cl)
set(CMAKE_AR llvm-lib)
set(CMAKE_LINKER lld-link)
set(CMAKE_MT llvm-mt)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(CMAKE_C_FLAGS_INIT "/vctoolsdir $ENV{HOME}/.local/share/xwin/crt /winsdkdir $ENV{HOME}/.local/share/xwin/sdk")
set(CMAKE_CXX_FLAGS_INIT "/vctoolsdir $ENV{HOME}/.local/share/xwin/crt /winsdkdir $ENV{HOME}/.local/share/xwin/sdk")
set(CMAKE_ASM_FLAGS_INIT "/vctoolsdir $ENV{HOME}/.local/share/xwin/crt /winsdkdir $ENV{HOME}/.local/share/xwin/sdk")
set(CMAKE_SHARED_LINKER_FLAGS_INIT "/vctoolsdir:$ENV{HOME}/.local/share/xwin/crt /winsdkdir:$ENV{HOME}/.local/share/xwin/sdk")
set(CMAKE_EXE_LINKER_FLAGS_INIT "/vctoolsdir:$ENV{HOME}/.local/share/xwin/crt /winsdkdir:$ENV{HOME}/.local/share/xwin/sdk")
