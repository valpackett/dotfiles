# https://www.milanvit.net/post/my-ultimate-shell-setup-with-fish-shell-and-tmux/

function _forget_cmd
    set -l cmd (commandline | string collect)
    printf "\nDo you want to forget '%s'? [Y/n]\n" $cmd
    switch (read | tr A-Z a-z)
        case n no
            commandline -f repaint
            return
        case y yes ''
            history delete --exact --case-sensitive -- $cmd
            if command -q atuin
                atuin search --delete $cmd
            end
            commandline ""
            commandline -f repaint
    end
end
