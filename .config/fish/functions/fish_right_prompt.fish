function fish_right_prompt
    if test -n "$SSH_CONNECTION"
        set_color -i green
        printf '@%s ' $hostname
        set_color normal
    end
    if test $status -ne 0
        set_color red
        printf '^%d^ ' $status
        set_color normal
    end
    if test $CMD_DURATION -ge 1000
        set_color brmagenta
        printf '{'
        if test $CMD_DURATION -ge 3600000
            printf '%02d:' (math -s0 $CMD_DURATION / 3600000)
        end
        if test $CMD_DURATION -ge 60000
            printf '%02d:' (math -s0 $CMD_DURATION \% 3600000 / 60000)
        end
        printf '%02d.%03d} ' (math -s0 $CMD_DURATION \% 60000 / 1000) (math -s0 $CMD_DURATION \% 1000)
        set_color normal
    end
    if test (count (jobs -p)) -gt 0
        set_color yellow
        printf '.%d. ' (count (jobs -p))
        set_color normal
    end
    set_color -d blue
    printf '%s' (date +%H:%M)
    set_color normal
end
