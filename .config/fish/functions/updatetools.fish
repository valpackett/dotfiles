function updatetools

    # General CLI life essentials (and not so essentials)
    set -af rusty eza # https://eza.rocks/ modern, maintained replacement for ls
    set -af rusty skim # https://github.com/lotabout/skim fuzzy finder in Rust
    set -af rusty atuin # https://atuin.sh/ magical shell history
    set -af rusty fd-find # https://github.com/sharkdp/fd alternative to 'find'
    set -af rusty broot # https://dystroy.org/broot/ new way to see and navigate directory trees
    set -af rusty xh # https://github.com/ducaale/xh friendly and fast tool for sending HTTP requests
    set -af rusty tealdeer # https://dbrgn.github.io/tealdeer/ tldr: simplified, example based and community-driven man pages
    set -af rusty rink # https://rinkcalc.app/about unit-aware calculator
    set -af rusty numbat-cli # https://numbat.dev/ statically typed scientific calculator
    set -af rusty pipe-rename # https://github.com/marcusbuffett/pipe-rename rename files using text editor
    set -af rusty kondo # https://github.com/tbillington/kondo cleans dependencies and build artifacts from your projects
    set -af rusty just # https://github.com/casey/just handy way to save and run project-specific commands
    set -af rusty jaq # https://github.com/01mf02/jaq JSON query (jq clone)
    set -af gogog github.com/mr-karan/doggo/cmd/doggo@latest # https://doggo.mrkaran.dev/docs/ DNS client

    # Potentially useful tools for various situations
    set -af rusty srgn # https://github.com/alexpovel/srgn code surgeon for precise text and code transplantation. A marriage of `tr`/`sed`, `rg` and `tree-sitter`
    set -af rusty grex # https://github.com/pemistahl/grex generating regular expressions from user-provided test cases
    set -af rusty tokei # https://github.com/XAMPPRocky/tokei count your code, quickly
    set -af rusty pastel # https://github.com/sharkdp/pastel generate, analyze, convert and manipulate colors
    set -af rusty hyperfine # https://github.com/sharkdp/hyperfine command-line benchmarking tool
    set -af rusty oha # https://github.com/hatoo/oha HTTP (2) benchmarking tool with nice UI
    set -af rusty caligula # https://github.com/ifd3f/caligula disk image writer with cool graphs
    set -af gogog github.com/mrmarble/termsvg/cmd/termsvg@latest # https://github.com/MrMarble/termsvg record, replay and export your terminal session to svg, with same format as asciinema
    set -af gogog github.com/charmbracelet/sequin@latest # https://github.com/charmbracelet/sequin explain ANSI escape sequences
    # (not on crates.io) # https://github.com/mogenson/ploot plot streaming data from stdin to a tty terminal

    # Search and replace tools
    set -af rusty ripgrep # https://github.com/BurntSushi/ripgrep recursively searches directories for a regex pattern while respecting your gitignore
    set -af rusty repgrep # https://github.com/acheronfail/repgrep interactive replacer for ripgrep
    set -af rusty ast-grep # https://github.com/ast-grep/ast-grep structural search and replace tool

    # Version control tools
    set -af rusty jj-cli # https://martinvonz.github.io/jj/ Jujutsu version control system
    set -af rusty difftastic # https://difftastic.wilfred.me.uk/ structural diff tool
    set -af rusty mergiraf # https://codeberg.org/mergiraf/mergiraf structural merge tool
    set -af rusty git-delta # https://github.com/dandavison/delta syntax-highlighting pager for git, diff, and grep output

    # Data format wrangling tools
    set -af rusty convfmt # https://github.com/oriontvv/convfmt convert between formats (json, yaml, toml, ron, json5)
    set -af rusty jless # https://jless.io/ JSON viewer designed for reading, exploring, and searching through JSON data
    set -af rusty bingrep # https://github.com/m4b/bingrep greps through binaries from various OSs and architectures, and colors them
    set -af rusty hexyl # https://github.com/sharkdp/hexyl hex viewer
    set -af rusty heh # https://github.com/ndd7xv/heh hex editor
    set -af rusty binwalk # https://github.com/ReFirmLabs/binwalk advanced format detector
    set -af gogog github.com/johnkerl/miller/v6/cmd/mlr@latest # https://miller.readthedocs.io/ like awk, sed, cut, join, and sort for name-indexed data such as CSV, TSV, and tabular JSON
    set -af gogog github.com/tomnomnom/gron@latest # https://github.com/tomnomnom/gron make JSON greppable
    set -af gogog github.com/wader/fq@master # https://github.com/wader/fq tool, language and decoders for working with binary and text formats

    # Multimedia tools
    set -af rusty ab-av1 # https://github.com/alexheretic/ab-av1 AV1 re-encoding using ffmpeg, svt-av1 & vmaf
    set -af rusty oxipng # https://github.com/shssoichiro/oxipng Multithreaded PNG optimizer written in Rust
    set -af pypyp beets # https://beets.readthedocs.io/ the music geek’s media organizer
    set -f pyinject_beets pyacoustid requests pylast beautifulsoup4
    set -af pypyp yt-dlp # https://github.com/yt-dlp/yt-dlp audio/video downloader
    set -af pypyp streamrip # https://github.com/nathom/streamrip scriptable music downloader
    set -af gogog git.sr.ht/~ft/unflac@latest # https://git.sr.ht/~ft/unflac fast frame accurate audio image + cue sheet splitting

    # Rust development tools
    set -af rusty cargo-edit # https://github.com/killercup/cargo-edit utility for managing cargo dependencies from the command line
    set -af rusty cargo-deny # https://github.com/EmbarkStudios/cargo-deny plugin for linting dependencies
    set -af rusty cargo-cache # https://github.com/matthiaskrgr/cargo-cache manage cargo cache, print sizes and remove dirs selectively
    set -af rusty cargo-bloat # https://github.com/RazrFalcon/cargo-bloat find out what takes most of the space in your executable
    set -af rusty cargo-outdated # https://github.com/kbknapp/cargo-outdated displaying when Rust dependencies are out of date

    ###############

    if [ (uname -s) = Linux ] && command -q gmake
        # workaround for native dep build scripts being surprised by Chimera Linux (well, not anymore, gmake is default now anyway)
        set -f wrap withgmake
    else
        set -f wrap env
    end

    if command -q cargo
        if not command -q cargo-install-update
            cargo install cargo-update
        end
        RUSTFLAGS='-C target-cpu=native' OPENSSL_NO_VENDOR=1 $wrap cargo install-update -i cargo-update $rusty
    else
        echo "No cargo :("
    end

    if command -q go
        go install github.com/Gelio/go-global-update@latest
        for pkg in $gogog
            $wrap go install $pkg
        end
        go-global-update
    else
        echo "No go :("
    end

    if command -q pipx
        for pkg in $pypyp
            $wrap pipx upgrade --install $pkg
            if set -q pyinject_$pkg
                $wrap pipx inject $pkg (string split ' ' (eval "echo \$pyinject_$pkg"))
            end
        end
    else
        echo "No pipx :("
    end
end
