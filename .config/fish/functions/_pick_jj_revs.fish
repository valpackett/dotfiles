# Only guaranteed to work with FUZZY=sk (https://github.com/lotabout/skim)

function _pick_jj_revs --description 'Search the output of jj log and preview revs. Replace the current token with the selected rev hash.'
    if not jj root >/dev/null 2>&1
        echo '_pick_jj_revs: Not in a jj repository.' >&2
    else
        set -f rev (commandline --current-token)
        test -n "$rev"; and set -f query "ancestors($rev)"; or set -f query 'all()'
        set -f jj_log_format 'format_short_id(change_id) ++ " - " ++ committer.timestamp().local().format("%Y-%m-%d") ++ "  " ++ description.first_line() ++ " " ++ bookmarks.map(|b| "[" ++ b ++ "]").join(" ") ++ "\n"'
        set -f selected_log_lines (
            jj log --color=always --no-graph -T $jj_log_format -r $query | \
            $FUZZY --ansi --multi --no-sort --layout=default --height=95% \
                --bind=ctrl-j:preview-up,ctrl-k:preview-down,shift-pgup:page-up,shift-pgdn:page-down,pgup:preview-page-up,pgdn:preview-page-down \
                --prompt='jj> ' --preview='jj show --color=always {1}'
        )
        if test $status -eq 0
            for line in $selected_log_lines
                set -f --append rev_hashes (string split --field 1 " " $line)
            end
            commandline --current-token --replace (string join ' ' $rev_hashes[-1..1])
        end
    end

    commandline --function repaint
end
