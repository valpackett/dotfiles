# Mostly https://github.com/PatrickF1/fzf.fish/blob/main/functions/_fzf_search_git_log.fish but:
# - not expanding hashes (why would you want that long stuff)
# - reverse output order (like for cherry-pick you need oldest first)
# - better colors
# Only guaranteed to work with FUZZY=sk (https://github.com/lotabout/skim)

function _pick_git_commits --description 'Search the output of git log and preview commits. Replace the current token with the selected commit hash.'
    if not git rev-parse --git-dir >/dev/null 2>&1
        echo '_pick_git_commits: Not in a git repository.' >&2
    else
        set -f rev (commandline --current-token)
        test -n "$rev"; or set -f rev 'HEAD'
        set -f git_log_format '%C(bold blue)%h%C(reset) - %C(magenta)%ad%C(reset) %C(yellow)%d%C(reset) %C(normal)%s%C(reset)  %C(green)[%an]%C(reset)'
        set -f selected_log_lines (
            git log --no-show-signature --color=always --format=format:$git_log_format --date=short $rev | \
            $FUZZY --ansi --multi --no-sort --layout=default --height=95% \
                --bind=ctrl-j:preview-up,ctrl-k:preview-down,shift-pgup:page-up,shift-pgdn:page-down,pgup:preview-page-up,pgdn:preview-page-down \
                --prompt='git> ' --preview='GIT_EXTERNAL_DIFF=difft DFT_COLOR=always DFT_DISPLAY=inline git show --color=always --stat --show-signature --ext-diff --patch {1}'
        )
        if test $status -eq 0
            for line in $selected_log_lines
                set -f --append commit_hashes (string split --field 1 " " $line)
            end
            commandline --current-token --replace (string join ' ' $commit_hashes[-1..1])
        end
    end

    commandline --function repaint
end
