set -U fish_features qmark-noglob # NOTE: universal var
set -gx EDITOR hx
set -gx FUZZY sk
set -gx LC_ALL en_US.UTF-8
set -gx LOCAL_MACHINE "$(uname -m)-$(string lower (uname -s))"
set -gx CARGO_INSTALL_ROOT "$HOME/.local/$LOCAL_MACHINE"
set -gx DENO_INSTALL "$HOME/.local"
set -gx DENO_INSTALL_ROOT "$HOME/.local"
set -gx GOPATH "$HOME/.cache/gopath" # ln -s ../../src ~/.cache/gopath/src; ln -s ../../.local/$LOCAL_MACHINE/bin ~/.cache/gopath/bin

if test -d /usr/local/share/terminfo # https://github.com/lotabout/skim/issues/412
    set -gx TERMINFO /usr/local/share/terminfo
end

fish_add_path -gm "$HOME/.config/netlify/helper/bin"
fish_add_path -gm "$HOME/.local/share/coursier/bin"
fish_add_path -gm "$HOME/.cargo/bin"
fish_add_path -gm "$HOME/.local/share/gem/ruby/"*/bin
fish_add_path -gm "$HOME/.local/bin"
fish_add_path -gm "$HOME/.local/$LOCAL_MACHINE/bin"
fish_add_path -gm "$HOME/.local/noarch-$(string lower (uname -s))/bin"

if status --is-interactive

    abbr -a g git
    abbr -a my ~/src/codeberg.org/valpackett/
    abbr -a p prevd

    # https://mitxela.com/projects/dotfiles_management
    alias dotgit='git --git-dir=$HOME/src/codeberg.org/valpackett/dotfiles --work-tree=$HOME'
    alias dottig='GIT_DIR=$HOME/src/codeberg.org/valpackett/dotfiles GIT_WORK_TREE=$HOME tig'

    function mcd
        mkdir -p $argv[1]
        cd $argv[1]
    end

    function multicd
        echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
    end
    abbr -a dotdot --regex '^\.\.+$' --function multicd

    function l
        eza -bTlL $argv[1] --icons --no-permissions --no-user --no-time $argv[2]
    end
    function eza_tree
        echo l (string sub -s 2 $argv[1])
    end
    abbr -a eza_tree --regex '^l\d+' --function eza_tree
    abbr -a l (eza_tree l1)

    function la
        eza -bTlL $argv[1] --icons --no-permissions --no-user --no-time -a $argv[2]
    end
    function eza_tree_all
        echo la (string sub -s 3 $argv[1])
    end
    abbr -a eza_tree_all --regex '^la\d+' --function eza_tree_all
    abbr -a la (eza_tree_all la1)

    function ll
        eza -bTlL $argv[1] --icons --octal-permissions --git --links -a $argv[2]
    end
    function eza_tree_long
        echo ll (string sub -s 3 $argv[1])
    end
    abbr -a eza_tree_long --regex '^ll\d+' --function eza_tree_long
    abbr -a ll (eza_tree_long ll1)

    bind \cz 'fg 2>/dev/null; commandline -f repaint'
    bind \cf _forget_cmd
    bind \cg _pick_git_commits
    bind \cv _pick_jj_revs
    bind \cj _go_to_project
    bind \ck _pick_procs

    function clqip -a file w h
        set -q w[1]; or set w 12
        set -q h[1]; or set h 10
        printf "data:image/webp;base64,%s" (cwebp -q 5 -m 6 -resize $w $h -quiet $file -o /dev/stdout | openssl base64 | tr -d '\n')
    end

    if command -q atuin
        atuin init fish --disable-up-arrow | source
    end

    # TODO: https://github.com/ivakyb/fish_ssh_agent but with better forwarding support
end
